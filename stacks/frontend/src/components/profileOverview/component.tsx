import './styles.scss';

import * as React from 'react';
import { OutboundLink } from 'react-ga';
import { Link } from 'react-router-dom';

import { Person } from '../../../../../lib/interfaces/Person';
import { ScholarlyArticle } from '../../../../../lib/interfaces/ScholarlyArticle';
import { ArticleList } from '../articleList/component';
import { OrcidButton } from '../orcidButton/component';
import { PageMetadata } from '../pageMetadata/component';

// tslint:disable-next-line:no-submodule-imports
const linkIcon = require('material-design-icons/content/svg/production/ic_link_48px.svg');
// tslint:disable-next-line:no-submodule-imports
const schoolIcon = require('material-design-icons/social/svg/production/ic_school_48px.svg');

interface Props {
  person: Partial<Person>;
  url: string;
  articles?: null | Array<Partial<ScholarlyArticle>>;
  currentUser?: string;
  profileIsCurrentUser?: boolean;
  isFlockademicUser?: boolean;
}

export const ProfileOverview: React.SFC<Props> = (props) => {
  return (
    <div itemScope={true} itemType="https://schema.org/Person">
      <section className="hero is-medium is-primary is-bold">
        <div className="hero-body">
          <div className="container">
            <PageMetadata
              url={props.url}
              title={props.person.name}
            />
            <h1 itemProp="name" className="title">{props.person.name}</h1>
          </div>
        </div>
      </section>
      {renderSuggestion(props)}
      {renderTopMatter(props)}
      {renderArticles(props)}
    </div>
  );
};

function renderTopMatter(props: Props) {
  const description = renderDescription(props.person.description);
  const aside = renderAside(props.person);

  if (description === null && aside === null) {
    return null;
  }

  return (
    <section className="section" data-test-id="topMatter">
      <div className="container">
        <div className="columns">
          {description}
          {aside}
        </div>
      </div>
    </section>
  );
}

function renderDescription(description?: string) {
  if (!description) {
    return null;
  }

  return (
    <div itemProp="description" className="column is-half-widescreen">
      {description}
    </div>
  );
}

function renderAside(person: Partial<Person>) {
  const affliations = renderEducationAndEmploymentInfo(person);
  const websites = renderWebsites(person.sameAs);

  if (
    websites === null
    && affliations === null
  ) {
    return null;
  }

  // If the description is shown, take up the remain space after a one-fifth margin.
  // Otherwise, only take up half the screen:
  const offsetClass = (person.description) ? 'is-offset-one-fifth-widescreen' : 'is-half-widescreen';

  return (
    <div className={`column ${offsetClass}`}>
      <aside className="panel">
        <p className="panel-heading">
          About {person.name}
        </p>
        {affliations}
        {websites}
      </aside>
    </div>
  );
}

function renderEducationAndEmploymentInfo(person: Partial<Person>) {
  if (!person.affliation) {
    return null;
  }

  // Only list current affliations
  const affliations = person.affliation.filter((affliation) => typeof affliation.endDate === 'undefined');

  if (affliations.length === 0) {
    return null;
  }

  return affliations.map((affliation, index) => {
    if (affliation.roleName) {
      return (
        <p itemProp="affliation" className="panel-block" key={`affliation_${index}`}>
          <span className="panel-icon">
            <img src={schoolIcon} alt=""/>
          </span>
          <span>
            <span itemProp="roleName">{affliation.roleName}</span>,&nbsp;
            <span itemProp="affliation">{affliation.affliation.name}</span>
          </span>
        </p>
      );
    }

    return (
      <p itemProp="affliation" className="panel-block" key={`affliation_${index}`}>
        <span className="panel-icon">
          <img src={schoolIcon} alt=""/>
        </span>
        <span itemProp="affliation">{affliation.affliation.name}</span>
      </p>
    );
  });
}

function renderWebsites(
  websites?: string | { sameAs: string; roleName?: string; } | Array<string | { sameAs: string; roleName?: string; }>,
) {
  if (!websites) {
    return null;
  }

  if (!Array.isArray(websites)) {
    websites = [ websites ];
  }

  // Do not list a link back to the ORCID profile; only include manually added websites.
  websites = websites.filter((website) => {
    return (typeof website === 'string')
      ? website.indexOf('orcid.org') === -1
      : website.sameAs.indexOf('orcid.org') === -1;
  });

  if (websites.length === 0) {
    return null;
  }

  return websites.map((website) => {
    const url = (typeof website === 'string') ? website : website.sameAs;
    const name = (typeof website === 'string') ? website : (website.roleName || website.sameAs);

    return (
      <p className="panel-block" key={url}>
        <span className="panel-icon">
          <img src={linkIcon} alt=""/>
        </span>
        <OutboundLink
          itemProp="sameAs"
          to={url}
          title={name}
          eventLabel="profile_sameAs"
          target="_blank"
        >
          {name}
        </OutboundLink>
      </p>
    );
  });
}

function renderArticles(props: Props) {
  if (typeof props.articles === 'undefined') {
    return null;
  }
  if (props.articles === null) {
    return (
      <div className="message is-danger content">
        <div className="message-body">
          Could not load articles by {props.person.name}, please try again.
        </div>
      </div>
    );
  }
  if (props.articles.length === 0) {
    return (
      <section className="section">
        <div className="container">
          <div className="columns">
            <div className="column is-half-widescreen">
              <h2 className="is-size-4">Articles by {props.person.name}</h2>
              <p>
                {props.person.name} has not&nbsp;
                <Link
                  to="/articles/new"
                  title="Submit a manuscript"
                >
                  shared research
                </Link>
                &nbsp;through Flockademic yet.
              </p>
            </div>
          </div>
        </div>
      </section>
    );
  }

  const submissionLink = (props.profileIsCurrentUser) ? '/articles/new' : undefined;

  return (
    <section className="section">
      <div className="container">
        <div className="columns">
          <div className="column is-half-widescreen">
            <h2 className="is-size-4">Articles by {props.person.name}</h2>
            <ArticleList articles={props.articles} submissionLink={submissionLink}/>
          </div>
        </div>
      </div>
    </section>
  );
}

function renderSuggestion(props: Props) {
  const suggestion = getSuggestion(props);

  if (suggestion === null) {
    return null;
  }

  return (
    <section className="section">
      <div className="container">
        {suggestion}
      </div>
    </section>
  );
}

function getSuggestion(props: Props) {
  if (
    props.isFlockademicUser === false
    && typeof props.currentUser === 'undefined'
  ) {
    return (
      <div className="message is-info content">
        <div className="message-body">
          This profile has not been claimed yet. Is this you?&nbsp;
          <OrcidButton>
            Sign in with ORCID
          </OrcidButton>
          &nbsp;to claim it.
        </div>
      </div>
    );
  }

  if (
    props.profileIsCurrentUser
    && props.articles
    && props.articles.length === 0
  ) {
    return (
      <div className="message is-info content">
        <div className="message-body">
          This is your profile, but it's rather empty right now. Give it some body;&nbsp;
          <Link
            to="/articles/new"
            title="Submit a new manuscript"
          >
            share your research
          </Link>.
        </div>
      </div>);
  }

  if (
    props.profileIsCurrentUser
    && props.articles
    && props.articles.length > 0
  ) {
    const campaignUrl =
      window.location.protocol + '//'
      + window.location.host
      + props.url
      // I've disabled the campaign parameters for now, as they look intimidating when Tweeting.
      // We might want to investigate whether and how much they actually negatively impact sharing:
      ;
      // + '?utm_campaign=permanent&utm_source=flockademic.com&utm_medium=twitter';

    const tweetText = encodeURIComponent('I\'m sharing my research on my @Flockademic profile:');
    const tweetLink = encodeURIComponent(campaignUrl);

    return (
      <div className="message is-info content">
        <div className="message-body">
          This is your profile—looks good!&nbsp;
          <OutboundLink
            to={`https://twitter.com/share?text=${tweetText}&url=${tweetLink}&hashtags=openaccess`}
            title="Share your profile on Twitter"
            eventLabel="profile-tweet"
            target="_blank"
          >
            Share it on Twitter.
          </OutboundLink>
        </div>
      </div>);
  }

  return null;
}
