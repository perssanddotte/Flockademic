require('./styles.scss');

import * as React from 'react';

import { Person } from '../../../../../lib/interfaces/Person';
import { PublishedScholarlyArticle } from '../../../../../lib/interfaces/ScholarlyArticle';
import { getProfiles } from '../../services/account';
import { getRecentScholarlyArticles } from '../../services/periodical';
import { ActivityTimeline } from '../activityTimeline/component';

interface State {
  articles?: null | PublishedScholarlyArticle[];
}

interface Props {
  title?: JSX.Element;
}

// The two consecutive promises in `componentDidMount` (`articles` and `profiles`) seem to trip up the tests:
/* istanbul ignore next */
export class ActivityTicker
  extends React.Component<
  Props,
  State
> {
  public state: State = {};

  constructor(props: any) {
    super(props);
  }

  public async componentDidMount() {
    try {
      let articles = await getRecentScholarlyArticles();

      if (articles.length > 0) {
        const profiles = await getProfiles(articles.map((article) => article.author[0].identifier));
        const profilesById = profiles.reduce<{ [ key: string ]: Person }>((soFar, profile) => {
          soFar[profile.identifier] = profile;

          return soFar;
        }, {});

        articles = articles.map((article) => {
          return {
            ...article,
            author: article.author.map((author: { identifier: string }) => profilesById[author.identifier]),
          };
        });
      }

      this.setState({ articles });
    } catch (e) {
      this.setState({ articles: null });
    }
  }

  public render() {
    if (!this.state.articles || this.state.articles.length < 5) {
      // Do not render anything when nothing could be fetched - this component is non-essential.
      // Also do not render with fewer than five items, as the fade-oud effect would be odd.
      return null;
    }

    return (
      <div className="activityTicker">
        {this.renderTitle()}
        <ActivityTimeline
          articles={this.state.articles}
        />
      </div>
    );
  }

  private renderTitle() {
    return this.props.title || null;
  }
}
