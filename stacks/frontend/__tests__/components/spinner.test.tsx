import { Spinner } from '../../src/components/spinner/component';

import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

it('should not show a spinner immediately', () => {
  const quip = shallow(<Spinner/>);

  expect(toJson(quip)).toMatchSnapshot();
});

it('should show a spinner after 200ms', () => {
  jest.useFakeTimers();
  const quip = mount(<Spinner/>);
  expect(quip).toHaveState('finishedDelay', false);

  jest.runTimersToTime(200);

  expect(quip).toHaveState('finishedDelay', true);
});

it('should clear timers if unmounted before they run out', () => {
  jest.useFakeTimers();
  const quip = mount(<Spinner/>);
  expect(quip).toHaveState('finishedDelay', false);

  jest.runTimersToTime(100);
  quip.unmount();

  expect(clearTimeout.mock.calls.length).toBe(1);
});

it('should support an inverted mode', () => {
  const quip = shallow(<Spinner invert={true}/>);

  quip.setState({ finishedDelay: true });

  expect(toJson(quip)).toMatchSnapshot();
});
