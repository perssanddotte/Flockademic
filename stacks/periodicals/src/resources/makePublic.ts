import {
  PostMakePeriodicalPublicRequest,
  PostMakePeriodicalPublicResponse,
} from '../../../../lib/interfaces/endpoints/periodical';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { SessionContext } from '../../../../lib/lambda/middleware/withSession';
import { isPeriodicalOwner } from '../../../../lib/utils/periodicals';
import { periodicalNameValidators, periodicalSlugValidators } from '../../../../lib/validation/periodical';
import { isValid } from '../../../../lib/validation/validators';
import { makePublic as service } from '../services/makePublic';
import { fetchPeriodical } from '../services/periodical';

export async function makePublic(
  context: Request<PostMakePeriodicalPublicRequest> & DbContext & SessionContext,
): Promise<PostMakePeriodicalPublicResponse> {
  if (!context.body || !context.body.object || !context.body.object.identifier) {
    throw new Error('No link to make the journal public at was specified.');
  }
  const proposedIdentifier = context.body.object.identifier;
  if (!isValid(proposedIdentifier, periodicalSlugValidators)) {
    throw new Error('The given journal link is not valid.');
  }
  // TODO Check whether `proposedIdentifier` is not used by another Periodical yet

  if (!context.body || !context.body.targetCollection || !context.body.targetCollection.identifier) {
    throw(new Error('No journal specified to make public.'));
  }
  const targetIdentifier = context.body.targetCollection.identifier;

  if (context.session instanceof Error) {
    throw new Error('You do not appear to be logged in.');
  }
  const session = context.session;

  if (!session.account) {
    throw new Error('You can only make a journal public if you have created an account.');
  }

  const storedPeriodical = await fetchPeriodical(context.database, targetIdentifier, context.session);

  const periodicalCreator = (storedPeriodical.creator) ? storedPeriodical.creator : undefined;
  if (
      storedPeriodical.creatorSessionId !== session.identifier
      && (!periodicalCreator || !isPeriodicalOwner({ creator: periodicalCreator }, context.session))
   ) {
    throw new Error('You can only make your own journals public.');
  }
  if (!isValid(storedPeriodical.name || '', periodicalNameValidators)) {
    throw new Error('The journal has to have a valid name before it can be made public.');
  }

  try {
    const result = await service(context.database, targetIdentifier, proposedIdentifier, session.account);

    return {
      result,
      targetCollection: {
        identifier: targetIdentifier,
      },
    };
  } catch (e) {
    // tslint:disable-next-line:no-console
    console.log('Database error:', e);

    throw new Error('There was a problem making the journal public, please try again.');
  }
}
