jest.mock('jsonwebtoken', () => ({
  verify: jest.fn().mockImplementation(() => ({ id: 'arbitrary account id' })),
}));
process.env.jwt_secret = 'mock_secret';

import { Account } from '../../interfaces/Account';
import { verifyJwt } from '../../lambda/verifyJwt';

it('should respond with an error when no JSON Web Token was provided', () => {
  expect(verifyJwt()).toEqual(new Error('You do not appear to be logged in.'));
});

it('should respond with an error when the JWT was invalid', () => {
  const mockedJsonwebtoken = require.requireMock('jsonwebtoken');
  mockedJsonwebtoken.verify.mockImplementationOnce(() => {
    throw new Error('Invalid JWT');
  });

  expect(verifyJwt('Arbitrary token')).toEqual(new Error('You do not appear to be logged in.'));
});

it('should validate the algorithm used to encrypt the JWT', () => {
  const mockedJsonwebtoken = require.requireMock('jsonwebtoken');

  verifyJwt('Arbitrary token');

  expect(mockedJsonwebtoken.verify.mock.calls.length).toBe(1);
  expect(mockedJsonwebtoken.verify.mock.calls[0][2].algorithms).toBeDefined();
  expect(mockedJsonwebtoken.verify.mock.calls[0][2].algorithms.indexOf('none')).toBe(-1);
});

it('should respond with an error if no JWT secret was set during deployment', () => {
  const oldJwtSecret = process.env.jwt_secret;
  delete process.env.jwt_secret;
  console.log = jest.fn();

  const response = verifyJwt('Arbitrary token');

  expect(console.log.mock.calls.length).toBe(1);
  expect(console.log.mock.calls[0][0])
    .toBe('Deployment error: no JSON Web Token secret was defined in $TF_VAR_jwt_secret');
  expect(response).toEqual(new Error('We are currently experiencing issues, please try again later.'));

  process.env.jwt_secret = oldJwtSecret;
});
